/**
 * Retrieve correct tier price for product on keyup of quantity for bulk buys
 *
 * When a keyup event occurs in the quantity field of a product, if the 
 * product has a bulky buy structure in place, create a AJAX call to the 
 * controller and output the result either into the price, or if an error has
 * occured, console.log the error message.
 */
$j(document).ready(function(){ 
    $j('input#qty').keyup(function(){
        if($j('.tier-prices').length > 0){
            if(typeof spConfig != "undefined"){
                prod = spConfig.getIdOfSelectedProduct();
            }
            else {
                prod = $j('input[type="hidden"][name="product"]').val();
            }
            $j.ajax({
                url:'/bulkpricing/index/bulk',
                data: {
                    prod: prod,
                    qty: $j('input#qty').val()
                },
                success: function(result){
                    if(result.indexOf('ERROR') == -1){
                        $j('.product-shop [id^="product-price-"] .price').html(result);
                    }   
                    else {
                        console.log(result);
                    }                 
                }
            });
        }
    });    
}); 