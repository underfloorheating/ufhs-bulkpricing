<?php

/**
 * Bulk Pricing Controller
 *
 * @version     $Id$
 * @package     Ufhs_Bulkpricing
 * @author      Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
 *
 */
class Ufhs_Bulkpricing_IndexController extends Mage_Core_Controller_Front_Action
{
	
	public function bulkAction()
	{
		try
		{
			// Confirm the required resources can be loaded and load them into variables
			$tierModel = Mage::getModel('bulkpricing/tiermodel');
			if(!$tierModel) 
			{
				throw new Exception(__('An error occured loading the requested resources.'));
			}

			// Assign the current currency symbol to a variable
			$currency_code = Mage::app()->getStore()->getCurrentCurrencyCode();
			$currency_symbol = Mage::app()->getLocale()->currency($currency_code)->getSymbol();			

			// Pull in the params and validate them
			$params = $tierModel->validateParams($this->getRequest()->getParams());			

			// Using the variables, get the correct tier price for that product and quantity
			$finalPrice = $tierModel->getFinalTierPrice($params['prod'],$params['qty']);
			echo $currency_symbol.number_format($finalPrice,2);			
			
		}		
		catch (Exception $e)
		{
			echo 'ERROR: '.$e->getMessage();
		}
		
	}

}